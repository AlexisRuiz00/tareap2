package Api.ApiPerson.service.impl;

import Api.ApiPerson.model.Person;
import Api.ApiPerson.repositorio.PersonRepository;
import Api.ApiPerson.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository pr;


    @Override
    public ResponseEntity<List<Person>> findAll() {
        return ResponseEntity.ok(pr.findAll());
    }

    @Override
    public ResponseEntity<List<Person>> findAll(int year1, int year2) {
        ArrayList<Person> list = new ArrayList<>();
        int max;
        int min;
        if (year1 > year2) {
            max = year1; min = year2;
        }else
            max=year2; min=year1;

        for(Person p : pr.findAll()){
            if (p.getDate().getYear() >= min && p.getDate().getYear() <= max)
                list.add(p);
        }
        return ResponseEntity.ok(list);
    }

    @Override
    public Person create(Person personVO) {
        return pr.save(personVO);
    }

    @Override
    public ResponseEntity<Person> update(Person person) {

        if (pr.existsById(person.getId())){
            return ResponseEntity.ok(pr.save(person));
        }else
          return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Boolean> delete(String id) {
        if (pr.existsById(id)){
            pr.deleteById(id);
            return ResponseEntity.ok(true);
        }else
            return ResponseEntity.notFound().build();

    }


}

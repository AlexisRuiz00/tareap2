package Api.ApiPerson.service;

import Api.ApiPerson.model.Person;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;

public interface PersonService {
    ResponseEntity<List<Person>> findAll();
    ResponseEntity<List<Person>> findAll(int year1,int year2);
    Person create(Person person);
    ResponseEntity<Person> update(Person person);
    ResponseEntity<Boolean> delete(String id);
}

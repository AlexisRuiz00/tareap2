package Api.ApiPerson.service.converter;

import Api.ApiPerson.model.Person;
import Api.ApiPerson.model.dto.PersonDTO;

public class PersonConverterToVO {

    public static Person composeVOFromDTO(PersonDTO personDTO) {
        return Person.builder().name(personDTO.getName()).surname(personDTO.getSurname())
                .phone(personDTO.getPhone()).email(personDTO.getEmail()).date(personDTO.getDate()).build();
    }

}

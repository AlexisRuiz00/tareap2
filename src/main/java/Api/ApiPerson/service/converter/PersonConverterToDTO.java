package Api.ApiPerson.service.converter;

import Api.ApiPerson.model.Person;
import Api.ApiPerson.model.dto.PersonDTO;

public class PersonConverterToDTO {

    public static PersonDTO composeDTOFromVO(Person person) {
        return PersonDTO.builder().name(person.getName()).surname(person.getSurname())
                .phone(person.getPhone()).email(person.getEmail()).date(person.getDate()).build();
    }

}

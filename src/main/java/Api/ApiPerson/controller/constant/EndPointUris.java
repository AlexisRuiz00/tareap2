package Api.ApiPerson.controller.constant;

public final class EndPointUris {
    public static final String API = "/api";
    public static final String V1 = "/v1";
    public static final String PERSON = "/person";
    public static final String ID = "/{id}";
    public static final String DATE = "/{year1}/{year2}";
}

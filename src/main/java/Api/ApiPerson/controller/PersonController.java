package Api.ApiPerson.controller;

import Api.ApiPerson.model.Person;
import Api.ApiPerson.model.dto.PersonDTO;
import Api.ApiPerson.service.PersonService;
import Api.ApiPerson.service.converter.PersonConverterToVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class PersonController implements PersonApi {
    @Autowired
    PersonService ps;

    @Override
    public ResponseEntity<List<Person>> findAll() {
        return ps.findAll();
    }

    @Override
    public ResponseEntity<List<Person>> findAll(int year1, int year2) {return ps.findAll(year1,year2);}

    @Override
    public ResponseEntity create(PersonDTO person) {
        System.out.println("ENTRA");
        return ResponseEntity.ok(ps.create(PersonConverterToVO.composeVOFromDTO(person)));
    }

    @Override
    public ResponseEntity<Person> update(String id, PersonDTO person) {
        Person personTmp = PersonConverterToVO.composeVOFromDTO(person);
        personTmp.setId(id);
        return ps.update(personTmp);
    }

    @Override
    public ResponseEntity<Boolean> deleteById(String id) {
        return null;
    }


}

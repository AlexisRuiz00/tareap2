package Api.ApiPerson.controller;

import Api.ApiPerson.controller.constant.EndPointUris;
import Api.ApiPerson.model.Person;
import Api.ApiPerson.model.dto.PersonDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(EndPointUris.API+EndPointUris.V1+EndPointUris.PERSON)
public interface PersonApi {

    @GetMapping
    ResponseEntity<List<Person>> findAll();

    @GetMapping(EndPointUris.DATE)
    ResponseEntity<List<Person>> findAll(@PathVariable("year1") int year1, @PathVariable("year2") int year2);

    @PostMapping
    ResponseEntity<Person> create(@RequestBody PersonDTO person);

    @PutMapping(EndPointUris.ID)
    ResponseEntity<Person> update(@PathVariable("id")  String id, @RequestBody PersonDTO person);

    @DeleteMapping(EndPointUris.ID)
    ResponseEntity<Boolean> deleteById(@PathVariable("id") String id);
}

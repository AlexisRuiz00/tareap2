package Api.ApiPerson.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.annotation.Documented;
import java.security.PublicKey;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document
public class Person {
    @Id
    String id;
    String name;
    String surname;
    String email;
    Long phone;
    Date date;
}
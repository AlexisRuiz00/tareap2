package Api.ApiPerson.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotBlank;
import java.util.Date;


@Getter
@Setter
@Builder

public class PersonDTO {
    @NotBlank
    @JsonProperty("name")
    String name;

    @NotBlank
    @JsonProperty("surname")
    String surname;

    @NotBlank
    @JsonProperty("email")
    String email;

    @NotBlank
    @JsonProperty("phone")
    Long phone;

    @NotBlank
    @JsonProperty("date")
    Date date;
}
